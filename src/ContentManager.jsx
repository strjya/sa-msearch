import React from 'react'
import 'Assets/CSS/App.css'
import TopNavigation from 'Components/TopNavigation'
import BottomNavigation from 'Components/BottomNavigation'
import TreatisesDisplay from 'Pages/TreatisesDisplay'
import Landing from 'Pages/Landing'
import CommandPage from 'Pages/CommandPage'
import Results from 'Pages/Results'
import Keywords from 'Pages/Keywords'
import Selector from 'Pages/Selector'
import Ending from 'Pages/Ending'
import Paper from '@material-ui/core/Paper'

function ContentManager(props) { // Gestisce quello che appare a schermo
  let target = false
  let forwardButton = null
  let backwardButton = null
  let title = false

  switch (props.page) {
    case 'command':
      target = <CommandPage {...props} />
      title = props.language.commandPage_title
      forwardButton = false
      backwardButton = null
      break
    case 'treatises':
      target = <TreatisesDisplay {...props} />
      title = props.language.treatises_title
      forwardButton = 'forward'
      backwardButton = 'backward'
      break
    case 'queryTreatises':
      target = <TreatisesDisplay {...props} />
      forwardButton = false
      backwardButton = 'backward'
      break
    case 'books':
      target = <Selector {...props} />
      title = props.language.books_title
      forwardButton = 'forward'
      backwardButton = 'backward'
      break
    case 'chapters':
      target = <Selector {...props} />
      title = props.language.chapters_title
      forwardButton = 'forward'
      backwardButton = 'backward'
      break
    case 'parts':
      target = <Selector {...props} />
      title = props.language.parts_title
      forwardButton = 'enter'
      backwardButton = 'backward'
      break
    case 'keywords':
      target = <Keywords {...props} />
      title = props.language.keywords_title
      forwardButton = 'forward'
      backwardButton = 'backward'
      break
    case 'results':
      target = <Ending {...props} />
      title = false
      forwardButton = false
      backwardButton = 'backward'
      break
    default:
      target = <Landing {...props} />
      title = false
      forwardButton = false
      backwardButton = false
      break
  }

  return (
    <div>
      <Paper style={{ paddingTop: 10, paddingBottom: 10 }}>
        <TopNavigation
          forwardButton={forwardButton}
          backwardButton={backwardButton}
          title={title}
          {...props}
        />
        {target}
        <BottomNavigation
          forwardButton={forwardButton}
          backwardButton={backwardButton}
          {...props}
        />
      </Paper>
      {props.page === 'results' ? <Results {...props} /> : null}
    </div>
  )
}

export default ContentManager
