import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import QueryBar from 'Components/QueryBar'
import ContentManager from './ContentManager'
import queryParser from 'Assets/queryParser'
import dataProcessor from 'Assets/dataProcessor'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import api from 'Assets/api.json'
import Header from 'Components/Header'
import HeaderLinks from 'Components/HeaderLinks'
import Axios from 'axios'
import _ from 'lodash'

// Language files
import it from 'Assets/languages/it.json'
import en from 'Assets/languages/en.json'

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    background: { paper: 'rgba(0,0,0,.4)' },
    primary: {
      light: '#484848',
      main: '#212121',
      dark: '#000000',
      contrastText: '#fff' },
    secondary: {
      light: '#ff5131',
      main: '#d50000',
      dark: '#9b0000',
      contrastText: '#fff' },
  },
  typography:
    {
      fontFamily: '"Book Antiqua", Palatino, serif',
    },
  overrides: {
    MuiInput: {
      root: {
        color: 'white',
      },
    },
  },
})

const navigator = {
  view:
    [
      'landing',
      'treatises',
      'books',
      'chapters',
      'parts',
      'ending',
    ],
  search:
    [
      'landing',
      'treatises',
      'keywords',
      'books',
      'chapters',
      'parts',
      'ending',
    ]
}

const initialState = {
  query: {
    command: '',
    keywords: {
      or: [
        { and: [], },
      ],
    },
    parameters: {
      books: [],
      chapters: [],
      parts: [],
    },
    treatises: []
  },
  page: 'landing',
  queryString: '',
  queryError: false,
}

const navigator = {
  view:
  [
    'command',
    'treatises',
    'books',
    'chapters',
    'parts',
    'results',
  ],
  search:
  [
    'command',
    'treatises',
    'keywords',
    'books',
    'chapters',
    'parts',
    'results',
  ],
}

const initialState = {
  query: {
    command: '',
    keywords: {
      or: [
        { and: [] },
      ],
    },
    parameters: {
      books: [],
      chapters: [],
      parts: [],
    },
    treatises: [],
  },
  page: 'landing',
  queryString: '',
  queryError: false,
  data: null,
}

class Wizard extends Component {
  constructor(props) {
    super(props)
    this.state = { // In caso di cambiamenti aggiornare initialState
      query: {
        command: '',
        keywords: {
          or: [
            { and: [] },
          ],
        },
        parameters: {
          books: [],
          chapters: [],
          parts: [],
        },
        treatises: [],
      },
      page: 'landing',
      queryString: '',
      queryError: false,
      data: null,
      loading: false,
      back: false, // this holds the previous page. false means "follow standard flux"
      language: it, // language state
    }
    this.handleLanguage = this.handleLanguage.bind(this)
    this.handleNavigation = this.handleNavigation.bind(this)
    this.handleQuery = this.handleQuery.bind(this)
    this.handleRead = this.handleRead.bind(this)
    this.queryParser = new queryParser(this.state.language)
    this.dataProcessor = new dataProcessor()
  }

  handleLanguage(language) {
    switch (language) {
      case 'it':
        this.setState({ language: it })
        break
      case 'en':
        this.setState({ language: en })
        break
      default:
        this.setState({ language: en })
    }
  }

  handleQuery() {
    this.setState({ loading: true })
    this.handleNavigation('results')
    Axios.post(api.url + '/getRawData', {
      params:
        {
          treatises: this.state.query.treatises,
          books: this.state.query.parameters.books,
          chapters: this.state.query.parameters.chapters,
          parts: this.state.query.parameters.parts,
        },
    }).then(response => {
      this.setState({ data: this.dataProcessor.process(response.data, this.state.query), loading: false })
    })
  }

  handleNavigation(target) { // imposta la pagina direttamente
    switch (target) {
      case 'forward':
        let nextPageIndex = navigator[this.state.query.command].indexOf(this.state.page) + 1
        nextPageIndex = (nextPageIndex >= navigator[this.state.query.command].length ? navigator[this.state.query.command].length : nextPageIndex)
        this.setState(prevState => ({
          page: navigator[prevState.query.command][nextPageIndex],
          back: prevState.page,
        }))
        break
      case 'backward':
        if (!this.state.queryError)
          if (this.state.back) {
            this.setState(prevState => ({ page: prevState.back, back: null }))
          } else {
            let prevPageIndex = navigator[this.state.query.command].indexOf(this.state.page) - 1
            prevPageIndex = (prevPageIndex >= 0 ? prevPageIndex : 0)
            this.setState({ page: navigator[this.state.query.command][prevPageIndex] })
          }

        break
      case 'enter':
        console.log('enter')
        this.handleQuery()
        break
      default:
        if (this.state.page === target)
          this.setState(prevState => ({ page: target, back: prevState.back }))
        else
          this.setState(prevState => ({ page: target, back: prevState.page }))
    }
  }

  handleRead(target, data, key) { // carica nello stato la scelta fatta nella schermata presente
    let query = this.state.query
    switch (target) {
      case 'command':
        query.command = data
        break
      case 'treatises':
      case 'queryTreatises':
        let treatises = query.treatises
        let indexOf = false
        for (let t in treatises)
          if (data.title === treatises[t].title &&
              data.author === treatises[t].author &&
              data.year === treatises[t].year)
            indexOf = t
        if (indexOf) treatises.splice(indexOf, 1)
        else treatises.push(data)
        query.treatises = treatises
        break
      case 'keywords':
        data = this.queryParser.keywordsCorrection(data)
        if (data !== '') { // Scrivo i dati normalmente
          query.keywords.or[key].and = data.split(' ')
          if (query.keywords.or[key + 1] === undefined) // Aggiungo la casella successiva
            query.keywords.or[key + 1] = { and: [] }
        } else if (query.keywords.or[key + 1] !== undefined) {
          query.keywords.or.splice(key, 1)
        }
        break
      case 'books':
      case 'chapters':
      case 'parts':
        query.parameters[target] = data
        break
      case 'query':
        data = data.value
        this.setState({
          queryString: data,
          queryError: this.queryParser.validateQuery(data),
        })
        if (this.queryParser.validateQuery(data)) {
          query = _.cloneDeep(initialState.query)
          query.treatises = this.state.query.treatises
        } else {
          query = this.queryParser.parseQuery(data)
          query.treatises = this.state.query.treatises
          if (query.keywords.or[query.keywords.or.length] === undefined)
            query.keywords.or[query.keywords.or.length] = { and: [] }
        }
        break
      case 'reset':
        query = _.cloneDeep(initialState.query)
        this.handleNavigation('landing')
        this.setState({ queryError: false, back: null })
        break
      default: // Caso per tutti i parametri
        if (data === '' || data === 0) // inserimento
          delete query.parameters[target]
        else // Rimozione per valore nullo
          query.parameters[target] = data
        break
    }
    this.setState({ query })
    if (target !== 'query' && target !== 'queryTreatises')
      this.setState({ queryString: this.queryParser.querify(query) })
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Header
          brand={(<img src={require('Assets/img/logo.png')} style={{ width: 85 }} />)}
          rightLinks={<HeaderLinks language={this.state.language} handleLanguage={this.handleLanguage} handleRead={this.handleRead} />}
          handleRead={this.handleRead}
          fixed
          color='dark'
          language={this.state.language}
        />
        <div style={{ height: 70 }} />
        <QueryBar
          handleNavigation={this.handleNavigation}
          handleRead={this.handleRead}
          queryString={this.state.queryString}
          queryError={this.state.queryError || this.state.query.treatises.length === 0}
          handleQuery={this.handleQuery}
          language={this.state.language}
          autoComplete={this.queryParser.autoComplete}
        />
        <br />
        <ContentManager
          handleNavigation={this.handleNavigation}
          handleRead={this.handleRead}
          page={this.state.page}
          query={this.state.query}
          data={this.state.data}
          loading={this.state.loading}
          language={this.state.language}
        />
      </MuiThemeProvider>
    )
  }
}

export default Wizard
