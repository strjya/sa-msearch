import React, { Component } from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'

class QueryBar extends Component {

  render() {
    return (
      <div>
  	    <Paper>
    			<TextField
    			  id = 'query'
    			  label = 'query'
    			  value = {this.props.queryString}
    			  onChange = {(event)=>this.props.handleRead('query', event.target.value)}
    			  margin = 'normal'
    			  fullWidth
    			  error = {this.props.queryError !== false}
                  onFocus = {()=>this.props.handleNavigation('queryTreatises')}
                  onKeyPress= {(ev)=>{if (ev.key==='Enter') {this.props.handleNavigation('ending')}}}
    			/>
    			<Button
    			  variant = 'raised'
    			  onClick = {()=>this.props.handleNavigation('ending')}
                  disabled = {!this.props.queryError}
                >
    			  Enter
    			</Button>
				<div>
				  {this.props.queryError}
				</div>
  		  </Paper>
      </div>
    )
  }
}



export default QueryBar;
