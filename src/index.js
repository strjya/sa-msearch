import React from 'react'
import ReactDOM from 'react-dom'
import Wizard from './Wizard'
import registerServiceWorker from './registerServiceWorker'

import { createBrowserHistory } from 'history'
import { Router, Route, Switch } from 'react-router'
import ReactGA from 'react-ga'

import indexRoutes from 'routes.jsx'

let hist = createBrowserHistory()
ReactGA.initialize('UA-75616966-2')

const logPageView = () => {
  window.scrollTo(0, 0)
  ReactGA.set({ page: window.location.pathname })
  ReactGA.pageview(window.location.pathname)
  return null
}

ReactDOM.render(
  <Router history={hist}>
    <div>
      <Route path='/' component={logPageView} />
      <Switch>
        {indexRoutes.map((prop, key) => {
          return <Route path={prop.path} key={key} component={prop.component} />
        })}
      </Switch>
    </div>
  </Router>,
  document.getElementById('root')
)
