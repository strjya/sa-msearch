class queryParser {
  constructor() {
    this.parseQuery = this.parseQuery.bind(this)
    this.validateQuery = this.validateQuery.bind(this)
  }

  /*
  Tre sezioni principali:
    - Controllo della query (correzione durante la scrittura e verifica dello stato di errore)
    - queryficazione dello stato (trasforma command, keywords e parameters in una stringa)
    - Statificazione della query (legge la query e la immette nello stato)
 */

  errorsCorrection(data) { // Corregge gli errori durante la digitazione
    this.mah = 'boh'
    let position
    let corrections = [
      { error: /^\s/, correction: '' },
      { error: /[-]\s?book(?!s|$)/, correction: '-books ', shift: 1 },
      { error: /[-]\s?chapter(?!s|$)/, correction: '-chapters ', shift: 1 },
      { error: /[-]\s?chap\s/, correction: '-chapters ', shift: 4 },
      { error: /[-]\s?part(?!s|$)/, correction: '-parts ', shift: 1 },
      { error: /[-]\s?flex(?!i|$)/, correction: '-flexibility ', shift: 7 },
      { error: /[-]\s?f/, correction: '-flexibility ', shift: 10 },
      { error: /[-]\s?d/, correction: '-distance ', shift: 7 },
      { error: /[-]\s?r/, correction: '-range ', shift: 4 },
      { error: /[-]\s?b(?!i|$)/, correction: '-books ', shift: 4 },
      { error: /[-]\s?c(?!i|$)/, correction: '-chapters ', shift: 7 },
      { error: /[-]\s?parts(?!i|$)/, correction: '-parts ', shift: 4 },
      { error: /\s\s/, correction: ' ' },
      { error: /[^A-Za-z0-9\s+-.]/, correction: '' },
      { error: /[-][-]/, correction: '-' },
      { error: /[.]{1,}(?=[0-9])/, correction: '..' },
      { error: /[.]{3,}/, correction: '..' },
      { error: /[,]/, correction: '' },
    ]

    position = data.selectionStart // Store cursor position

    for (let r in corrections) // Scan the array of objects
      if (data.value.match(corrections[r].error)) { // Replace the errors
        data.value = data.value.replace(corrections[r].error, corrections[r].correction)
        if (corrections[r].shift) // Increment cursor position
          position = position + corrections[r].shift
      }

    data.value = data.value.toLowerCase()

    data.selectionEnd = position // Return cursos position

    return data.value
  }

  autoComplete(data) { // Corregge gli errori durante la digitazione
    let knownCommands = [
      'view',
      'search',
      'context',
      'books',
      'chapters',
      'parts',
      'range',
      'distance',
      'flexibility',
    ]
    let fragData = data.value.toLowerCase().split(' ')
    let letters = data.value.split('')
    let index = 0
    let i = 0
    while (i < data.selectionStart) {
      if (letters[i] === ' ') index++
      i++
    }
    let commandToComplete = fragData[index].replace('-', '')
    let possibleCompletions = knownCommands.filter((x) => x.startsWith(commandToComplete))
    if (possibleCompletions.length === 1) {
      let regex = new RegExp(commandToComplete, 'g')
      data.selectionEnd = data.selectionStart + possibleCompletions[0].length - commandToComplete.length // Return cursos position
      fragData[index] = fragData[index].replace(commandToComplete, possibleCompletions[0])
      data.value = fragData.join(' ')
      return data
    }
    return data
  }

  keywordsCorrection(query) { // correction for the keywords from the wizard
    let corrections = [
      { error: /[-][,][.]/, correction: '' },
      { error: /[' ']{2,}/, correction: ' ' },
      { error: /^\s/, correction: '' },
      { error: /\s\s/, correction: ' ' },
      { error: /[0-9]/, correction: ' ' },
    ]

    for (let r in corrections)
      query = query.replace(corrections[r].error, corrections[r].correction)

    return query.toLowerCase()
  }

  validateQuery(query) { // Valida la query
    let conditions = [
      { exp: /[^a-z0-9\s+-.]/, res: 'query_error01' }, // Si possono usare solo lettere, numeri, . , + e -
      { exp: /^(?!view|search|$)/, res: 'query_error02' }, // La ricerca deve iniziare con un comando
      { exp: /view(?=\s[a-z])/, res: 'query_error03' }, // Con il comando view non servono keywords
      { exp: /search(?=\s[-]|$|\s$)/, res: 'query_error04' }, // Con il comando search serve almeno una keyword
      { exp: /\s{2,}/, res: 'query_error05' }, // Attenzione ai doppi spazi
      // Regole per i numeri
      { exp: /\D[0]/, res: 'query_error06' }, // Il valore 0 non è accettato
      // Regole per il -
      { exp: /[^\s](?=[-])/, res: 'query_error07' }, // Manca uno spazio prima del trattino
      // Regole per il +
      { exp: /[^a-z](?=[+])/, res: 'query_error08' }, // Il + serve solo ad unire le keywords
      { exp: /[+](?!$|[a-z])/, res: 'query_error09' }, // Il + serve solo ad unire le keywords
      { exp: /[-](?=\w*(?=[+]))/, res: 'query_error10' }, // Il + serve solo ad unire le keywords
      // Regole per il -
      { exp: /[^0-9.][.]/, res: 'query_error11' }, // Il punto serve solo a separare i valori
      { exp: /[.](?![0-9]|$|[.])/, res: 'query_error12' }, // Il punto serve solo a separare i valori
      { exp: /[0-9][.](?![.]|$)/, res: 'query_error13' }, // Manca un punto tra il primo valore e l`ultimo
      // Regole per i parametri
      { exp: /[-](?![a-z]|$|\s)/, res: 'query_error14' }, // Manca il parametro
      { exp: /[-][a-z0-9 ]{1,}\s[a-z]/, res: 'query_error15' }, // Dopo il primo parametro non sono ammesse keywords
      { exp: /[-][a-z]*[\d]/, res: 'query_error16' }, // Manca lo spazio tra il parametro e il suo valore
      { exp: /[-][a-z]*[-]/, res: 'query_error17' }, // Manca uno spazio tra un parametro e il - successivo
    ]

    for (let c in conditions)
      if (query.match(conditions[c].exp))
        return conditions[c].res

    return this.validateParameters(query)
  }

  validateParameters(query) { // Controllo sulla validità dei parametri
    let validParameters = [
      //      /[a-z](?=$)/, //evita l'errore mentre si scrive
      /books\b/,
      /chapters\b/,
      /parts\b/,
      /range\b/,
      /flexibility\b/,
      /distance\b/,
      /clean\b/,
      /compact\b/,
      /english\b/,
    ]

    let parametersArray = query.split(/\s[-]/) // estrae dalla query i parametri
    parametersArray.splice(0, 1)
    if (parametersArray.length < 1) return false

    for (let p in parametersArray)
      for (let v in validParameters)
        if (parametersArray[p].match(validParameters[v])) parametersArray[p] = ''

    parametersArray = parametersArray.filter(Boolean)
    if (parametersArray.length > 0)
      return 'Il parametro "' + parametersArray[0] + '" non è valido'
    return false
  }

  querify(obj) { // Trasforma lo stato.query in una stringa.
    return '' +
            obj.command +
            obj.keywords.or.map((x) => {
              if (x.and.length === 0) return ''
              else return ' '.concat(x.and.toString().replace(/,/g, '+'))
            })
              .toString().replace(/,/g, '') +
            Object.keys(obj.parameters).map((x) => {
              if (obj.parameters[x] === null || // Se il parametro è vuoto
                    obj.parameters[x] === false ||
                    (obj.parameters[x].length < 1))
                return ''
              if (Array.isArray(obj.parameters[x])) // Se è un array
                return ' -' + x + this.querifyParameters(obj.parameters[x])
              else // Se è un valore singolo
                return ' -' + x + ' ' + obj.parameters[x]
            }).filter(Boolean).toString().replace(/,/g, '')
  }

  querifyParameters(parametersArray) { // restituisce una stringa
    let valuesArray = []
    let result = ''
    for (let h in parametersArray)
      if (h === 0) valuesArray[0] = parametersArray[h]
      else if (parametersArray[h] === (parametersArray[h - 1] + 1))
        valuesArray[valuesArray.length - 1].push(parametersArray[h])
      else valuesArray[valuesArray.length] = [(parametersArray[h])]

    for (let j in valuesArray)
      if (valuesArray[j].length > 1)
        result = result + ' ' + valuesArray[j][0] + '..' + valuesArray[j][valuesArray[j].length - 1]
      else result = result + ' ' + valuesArray[j]

    return result
  }

  parseQuery(query) { // Trasforma la query in un oggetto stato.query
    if (!(this.validateQuery(query))) // utilizzo il campo error per vedere se la query è a posto o no
  		return {
  		  command: this.extractCommand(query), // view | search | context
  		  keywords: this.extractKeywords(query), // oggetti inseriti dall'utente or:[{and: ['spada']}]
  		  parameters: this.extractParameters(query), // book & chapter & part
      }
	  else return false
  }

  extractCommand(query) { // Estrae i comandi Search e View
    return query.split('-')[0].split(' ')[0]
  }

  extractKeywords(query) { // Estra le keywords
    let keywordsArray = query.trim().split('-')[0].trim().split(' ')
    keywordsArray.splice(0, 1)
    if (keywordsArray.length > 0)
      return this.processKeywords(keywordsArray)
    return { or: [{ and: [] }] }
  }

  processKeywords(keywordsArray) { // Inserisce le keyword in un oggetto
    let result = { or: [] }
    for (let i in keywordsArray)
      result.or.push({ and: keywordsArray[i].trim().split('+') })

    return result
  }

  extractParameters(query) { // Estrae i parametri  Seguito da tre funzioni process
    let parametersArray = query.trim().split(' -')
    parametersArray.splice(0, 1)
    if (parametersArray.length === 0)
      return {
        books: [],
        chapters: [],
        parts: [],
      }
    return this.processParameters(parametersArray)
  }

  processParameters(parametersArray) {
    let normalParametersArray = []
    let specialParametersArray = []
    for (let p in parametersArray)
      if (parametersArray[p].substr(0, 4) === 'book' ||
          parametersArray[p].substr(0, 4) === 'chap' ||
          parametersArray[p].substr(0, 4) === 'part')
        specialParametersArray.push(parametersArray[p])
      else
        normalParametersArray.push(parametersArray[p])

    return Object.assign(this.processSpecialParameters(specialParametersArray),
      this.processNormalParameters(normalParametersArray))
  }

  processNormalParameters(parametersArray) {
    let result = {}
    for (let p in parametersArray) {
      let param = parametersArray[p].trim().split(' ')
      if (param.length > 1)
        result[param[0].trim()] = Number(param[1].trim())
      else return false
    }
    return result
  }

  processSpecialParameters(parametersArray) {
    let result = {
      books: [],
      chapters: [],
      parts: [],
    }
    for (let p in parametersArray) {
      let param = parametersArray[p].trim().split(' ')
      if (param.length > 1) {
        let name = param[0]
        let value = param // estraggo i valori
        let valuesTemp = []
        value.splice(0, 1) // tolgo il parametro

        for (let a in value) // separo i valori, li converto in numeri singoli progressivi
          if (value[a].match(/[.][.]/)) {
            let confines = value[a].split('..').map(Number).filter(Boolean).sort((a, b) => a - b)
            for (let c = confines[0]; c <= confines[1]; c++)
              valuesTemp.push(Number(c))
          } else { value[a] = Number(value[a]) }

        // Eliminare le stringhe, aggiungere i valori estratti, mettere in ordine ed eliminare i doppioni
        value = value.filter(Number).concat(valuesTemp).sort((a, b) => a - b).filter((v, i, a) => a.indexOf(v) === i)
        result[name] = value
      }
    }
    return result
  }
}

export default queryParser
