import levenshtein from 'fast-levenshtein'

class dataProcessor {
  constructor() {
    this.process = this.process.bind(this)
  }

  process(rawData, query) {
    switch (query.command) {
      case 'search':
        return this.structureData(
          this.processRange(
            this.processDistance(
              this.processSearch(rawData, query)
              , query)
            , query)
          , query)
      case 'view':
        return this.structureDataView(rawData, query)
      default:
        return null
    }
  }
  //* **!!! MISSING TITLE ANALYSIS !!!***
  processSearch(rawData, query) {
    this.mah = 'boh'
    const flex = (query.parameters.flexibility == null ? 0 : query.parameters.flexibility)
    // process row by row
    for (let x = rawData.length - 1; x >= 0; x--) {
      let r = rawData[x]
      r.structuredText = r.text.replace(/'/g, ' \' ').replace(/\n/g, ' \n ').split(' ').map(x => ({ word: x, cleanWord: x.replace(/[^a-zA-Z 0-9]+/g, '').toLowerCase(), keyword: false, hidden: false }))
      r.sequences = []
      let hasMatches = false
      for (const or of query.keywords.or)
        if (or.and.length > 0) {
          // save the sequence of valid keywords
          let sequence = []
          // pointer to the current target keyword in or.and
          let currentTarget = 0
          // cycle through the structured text
          for (let i = 0; i < r.structuredText.length; i++) {
            let w = r.structuredText[i]
            // check clean word for matching --> use levenshtein matching
            let revertionCounter = 0
            // try matching all keywords starting from current target.
            while ((revertionCounter <= currentTarget) && levenshtein.get(w.cleanWord, or.and[currentTarget - revertionCounter]) > flex)
              revertionCounter++
            if (revertionCounter <= currentTarget) {
              // match!
              // revert sequence to keyword found, and reset current target
              for (let a = 0; a < revertionCounter; a++) {
                sequence.splice(-1, 1)
                currentTarget--
              }
              // save corresponding keyword
              w.cleanWord = or.and[currentTarget]
              sequence.push(i)
              currentTarget = (currentTarget + 1 < or.and.length ? currentTarget + 1 : 0)
            }
          }
          // eliminate uncompleted sequences
          let validSequenceTotalLenght = sequence.length - sequence.length % or.and.length
          let newSequence = []
          for (let i = 0; i < validSequenceTotalLenght; i++)
            newSequence.push(sequence[i])

          // sequence is created!
          // if the sequence is empty -> remove this row
          if (newSequence.length > 0)
            hasMatches = true
          // now mark the keywords
          let chunk = or.and.length
          for (let i = 0; i < newSequence.length; i += chunk)
            r.sequences.push([...newSequence.slice(i, i + chunk)])
        }

      if (!hasMatches)
        rawData.splice(x, 1)
    }
    console.log(rawData)
    return rawData
  }

  processDistance(rawData, query) {
    this.mah = 'boh'
    const distance = (query.parameters.distance == null ? 1 : query.parameters.distance + 1)
    for (let x = rawData.length - 1; x >= 0; x--) {
      let r = rawData[x]
      let keywordList = query.keywords.or.map(x => x.and.reduce((x, y) => x + ' ' + y, '').trim()).filter(Boolean)
      r.keywords = {}
      for (let k of keywordList)
        r.keywords[k] = 0

      let keywordCount = 0
      // itarate over words
      for (let index in r.sequences) {
        let sequence = r.sequences[index]
        // check the chain
        keywordCount++
        let maxDistance = 0
        for (let i = 0; i < sequence.length - 1; i++)
          if (sequence[i + 1] - sequence[i] > maxDistance)
            maxDistance = sequence[i + 1] - sequence[i]
        console.log(sequence)
        if (maxDistance > distance) {
          // clean the sequence
          keywordCount--
          r.sequences.splice(index, 1)
        } else {
          r.keywords[sequence.map(x => r.structuredText[x].cleanWord).join(' ')]++
        }
      }
      if (keywordCount === 0)
        rawData.splice(x, 1)
      else
        for (let sequence of r.sequences)
          for (let word of sequence)
            r.structuredText[word].keyword = true
    }
    return rawData
  }

  processRange(rawData, query) {
    this.mah = 'boh'
    const range = (query.parameters.range == null ? 0 : query.parameters.range)
    if (range > 0)
      for (let r of rawData) {
        let keywordPointersList = []
        let index = 0
        // itarate over words
        while (index < r.structuredText.length) {
          let w = r.structuredText[index]
          if (w.keyword)
            keywordPointersList.push(index)
          index++
        }
        index = r.structuredText.length - 1
        // itarate over words
        let next = keywordPointersList.length - 1
        let prev = (next - 1 > 0 ? next - 1 : 0)
        while (index >= 0) {
          // remove too far words
          if (Math.abs(index - keywordPointersList[prev]) > range && Math.abs(keywordPointersList[next] - index) > range && !r.structuredText[index].keyword)
            r.structuredText[index].hidden = true

          // update keyword indexes
          if (index <= keywordPointersList[prev]) {
            next = (next !== prev ? next - 1 : next)
            prev = (prev > 0 ? prev - 1 : prev)
          }
          index--
        }
      }

    return rawData
  }

  structureData(rawData, query) {
    this.mah = 'boh'
    // build result object
    /* result obj example:
      [
        xxyyzz :{
          title: 'xxx',
          author: 'yyy',
          year: zz,
          books:
            {
              xx :{
                    number: xx,
                    title: 'xx',
                    chapters:
                      {
                        xx :{
                          number: xx,
                          title: 'xx',
                          parts:
                            {
                              xx :{
                                number: xx,
                                title: 'xx',
                                text: 'ciwefiwejnkl',
                                transcriber1: 'ff'
                                transcriber2: 'rr'
                              },
                            }
                        },
                      }
                  },
            },
        },
      ]
    */
    // create skeleton
    let response = {}
    for (let t of query.treatises)
      response[t.author + t.title + t.year] = { title: t.title, author: t.author, year: t.year, books: {}, keywords: {} }
    for (let r of rawData) {
      let treatiseIndex = r.author + r.title + r.year
      // merge keywords into treatise
      for (const k in r.keywords)
        if (!response[treatiseIndex].keywords[k])
          response[treatiseIndex].keywords[k] = r.keywords[k]
        else
          response[treatiseIndex].keywords[k] += r.keywords[k]
      // if book does not exist, create it
      if (!(r.book in response[treatiseIndex].books))
        response[treatiseIndex].books[r.book] = { number: r.book, title: r.bookTitle, chapters: {}, keywords: {} } // add book
      // merge keywords into book
      for (const k in r.keywords)
        if (!response[treatiseIndex].books[r.book].keywords[k])
          response[treatiseIndex].books[r.book].keywords[k] = r.keywords[k]
        else
          response[treatiseIndex].books[r.book].keywords[k] += r.keywords[k]
      // if chapter does not exist, create it
      if (!(r.chapter in response[treatiseIndex].books[r.book].chapters))
        response[treatiseIndex].books[r.book].chapters[r.chapter] = { number: r.chapter, title: r.chapterTitle, parts: {}, keywords: {} } // add chapter
      // merge keywords into chapter
      for (const k in r.keywords)
        if (!response[treatiseIndex].books[r.book].chapters[r.chapter].keywords[k])
          response[treatiseIndex].books[r.book].chapters[r.chapter].keywords[k] = r.keywords[k]
        else
          response[treatiseIndex].books[r.book].chapters[r.chapter].keywords[k] += r.keywords[k]
      // add part
      response[treatiseIndex].books[r.book].chapters[r.chapter].parts[r.part] = { number: r.part, title: r.partTitle, text: r.structuredText, transcriber1: r.transcriber1, transcriber2: r.transcriber2, keywords: r.keywords } // add part
    }
    return response
  }

  structureDataView(rawData, query) { // used to resolve the bug of r.structuredText. To merge
    this.mah = 'boh'
    // create skeleton
    let response = {}
    for (let t of query.treatises)
      response[t.author + t.title + t.year] = { title: t.title, author: t.author, year: t.year, books: {}, keywords: {} }
    for (let r of rawData) {
      let treatiseIndex = r.author + r.title + r.year
      if (!(r.book in response[treatiseIndex].books))
        response[treatiseIndex].books[r.book] = { number: r.book, title: r.bookTitle, chapters: {}, keywords: {} } // add book
      if (!(r.chapter in response[treatiseIndex].books[r.book].chapters))
        response[treatiseIndex].books[r.book].chapters[r.chapter] = { number: r.chapter, title: r.chapterTitle, parts: {}, keywords: {} } // add chapter
      response[treatiseIndex].books[r.book].chapters[r.chapter].parts[r.part] = { number: r.part, title: r.partTitle, text: r.text, transcriber1: r.transcriber1, transcriber2: r.transcriber2, keywords: {} } // add part
    }
    return response
  }
}
export default dataProcessor
