import jsPDF from 'jspdf'
import watermark from 'Assets/img/watermark'

const yTitleOffset = 10
const yBookOffset = 7
const startingY = 30
const yLineOffset = 5
const xMax = 170
const yMax = 270
const maxSpaceOnNewPart = 30
const maxSpaceOnNewChapter = 30
const maxSpaceOnNewBook = 70
const bookTopMargin = 30
const chapterTopMargin = 20
const partTopMargin = 10

class PDFgenerator {
  constructor() {
    this.generatePDF = this.generatePDF.bind(this)
    this.query = null
  }

  setQuery(query) {
    this.query = query
  }

  generatePDF(data, query) {
    let doc = new jsPDF({
      orientation: 'p',
      unit: 'mm',
      format: 'a4',
    })
    this.setQuery(query)
    let y = startingY
    let x = 20
    let first = true
    doc.setFont('Book Antiqua', 'normal')
    this.insertCopyrigth(doc)
    doc.addImage(watermark.uri, 'JPEG', 0, 0, 210, 297, 'watermark')
    this.insertHeading(doc)
    for (const t in data) {
      let treatise = data[t]
      y = this.printTreatiseTitle(first, treatise.author + ', ' + treatise.title + ', ' + treatise.year, x, y, doc)
      first = false
      y = this.printBooks(treatise.books, x, y, doc)
    }
    // doc.output('dataurlnewwindow')
    doc.save('MSearch-results.pdf')
  }

  printBooks(books, x, y, doc) {
    doc.setFontSize(12)
    for (const b in books) {
      let book = books[b]
      y = this.printBookTitle(book.number, book.title, x, y, doc)
      y = this.printChapters(book.chapters, x, y, doc)
    }
    return y
  }

  printChapters(chapters, x, y, doc) {
    doc.setFontSize(12)
    for (const c in chapters) {
      let chapter = chapters[c]
      y = this.printChapterTitle(chapter.number, chapter.title, x, y, doc)
      y = this.printParts(chapter.parts, x, y, doc)
    }
    return y
  }

  printParts(parts, x, y, doc) {
    doc.setFontSize(10)
    for (const p in parts) {
      let part = parts[p]
      y = this.printPartTitle(part.number, part.title, x, y, doc)
      y = this.printText(this.extractText(part), x, y, doc)
    }
    return y
  }

  extractText(part) { // choose how to extract the Text from the parts
    switch (this.query.command) {
      case 'search':
        return this.composeText(part.text)
      case 'view':
        return part.text
      default:
        return ''
    }
  }

  printText(text, x, y, doc) {
    let splitText = doc.splitTextToSize(text, xMax)
    if (y > yMax) {
      y = startingY
      this.addPage(doc)
    }
    doc.setFontSize(10)
    doc.setFontStyle('normal')
    for (const t of splitText) {
      doc.text(t, x + xMax / 2, y, 'center')
      y += yLineOffset
      if (y > yMax) {
        y = startingY
        this.addPage(doc)
      }
    }
    return y
  }

  printPartTitle(number, title, x, y, doc) {
    if (number === 1 && title === '') return y
    let splitText = doc.splitTextToSize(title, xMax)
    y += partTopMargin
    if (y > yMax - maxSpaceOnNewPart) {
      y = startingY + partTopMargin
      this.addPage(doc)
    }
    doc.setFillColor(0, 0, 0)
    doc.circle(xMax - 12, y - 1.5, 4, 'F')
    doc.rect(xMax - 12, y - 5.5, 100, 8, 'F')
    doc.setTextColor(255, 255, 255)

    doc.setFontSize(12)
    doc.setFontStyle('bold')
    doc.text('Parte ' + number, xMax - 10, y)
    doc.setTextColor(0, 0, 0)
    for (const t of splitText) {
      doc.text(t, x, y)
      y += yLineOffset
      if (y > yMax) {
        y = startingY
        this.addPage(doc)
      }
    }
    y += 4
    return y
  }

  printChapterTitle(number, title, x, y, doc) {
    let splitText = doc.splitTextToSize(title, xMax - 70)
    y += chapterTopMargin
    if (y > yMax - maxSpaceOnNewChapter) {
      y = startingY + chapterTopMargin
      this.addPage(doc)
    }
    doc.setFillColor(153, 0, 0)
    doc.circle(x + 10, y - 10, 10, 'F')
    doc.setFontStyle('bold')
    doc.setTextColor(255, 255, 255)
    doc.setFontSize(20)
    y -= 7.5
    doc.text(x + 8 - ((number + '').length - 1) * 1.5, y, number + '')
    doc.setTextColor(0, 0, 0)
    doc.setFontSize(14)
    for (const t of splitText) {
      doc.text(t, x + 30, y)
      y += yLineOffset
      if (y > yMax) {
        y = startingY
        this.addPage(doc)
      }
    }
    y += 7.5
    return y
  }

  printBookTitle(number, title, x, y, doc) {
    let splitText = doc.splitTextToSize(title, xMax)
    y += (number === 1 ? 0 : bookTopMargin)
    if (y > yMax - maxSpaceOnNewBook) {
      y = startingY + (number === 1 ? 0 : bookTopMargin)
      this.addPage(doc)
    }
    doc.setFillColor(0, 0, 0)
    doc.circle(xMax - 12, y - 1.5, 4, 'F')
    doc.rect(0, y - 5.5, xMax - 12, 8, 'F')
    doc.setTextColor(255, 255, 255)

    doc.setFontSize(14)
    doc.setFontStyle('bold')
    doc.text('Libro ' + number, x, y)
    for (const t of splitText) {
      doc.text(t, (x + xMax) / 2, y)
      y += yLineOffset
      if (y > yMax) {
        y = startingY
        this.addPage(doc)
      }
    }
    doc.setTextColor(0, 0, 0)
    return y
  }

  printTreatiseTitle(first, title, x, y, doc) {
    let splitText = doc.splitTextToSize(title, xMax)
    if (!first) {
      y = startingY + bookTopMargin
      this.addPage(doc)
    }
    doc.setFontSize(16)
    doc.setFontStyle('normal')
    for (const t of splitText) {
      doc.text(t, x, y)
      y += yTitleOffset
    }
    return y
  }

  addPage(doc) {
    doc.addPage()
    // insert watermark
    doc.addImage(watermark.uri, 'JPEG', 0, 0, 210, 297, 'watermark')
    // insert copyright information
    this.insertCopyrigth(doc)
    this.insertHeading(doc)
  }

  insertCopyrigth(doc) {
    doc.setFontSize(9)
    doc.setFontStyle('italic')
    doc.setTextColor(160, 160, 160)
    doc.text('©' + (new Date().getFullYear()) + ', Sword Academy - Associazione Sportiva Dilettantistica', 20, 280)
    doc.text('This document and it\'s content can only be distributed accompanied by a clear statement on its source.', 20, 285)
    doc.setTextColor(0, 0, 0)
  }

  insertHeading(doc) {
    doc.setFontSize(9)
    doc.setFontStyle('italic')
    doc.setTextColor(160, 160, 160)
    doc.text('Generated using the Master Search', 20, 20)
    doc.text('http://msearch.sword.academy', xMax - 20, 20)
    doc.setTextColor(0, 0, 0)
  }

  composeText(text) { // Render [text]
    return text.map((x, index, array) => {
      if (x.hidden) return ''
      let word = (x.keyword
        ? x.word.toUpperCase() + ' '
        : x.word + (array[index + 1]
          ? (x.word === '\'' || array[index + 1].word === '\'' ? '' : ' ')
          : ' '))
      return word
    }).reduce((x, y) => x + y, '')
  }
}

export default PDFgenerator
