import React, { Component } from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper'
import Checkbox from '@material-ui/core/Checkbox';

const treatises = [
  {year: 1531, author: 'Antonio Manciolino', title: 'Opera Nova'},
  {year: 1536, author: 'Achille Marozzo', title: 'Opera Nova'},
  {year: 1570, author: 'Giacomo di Grassi', title: 'Ragione di adoprar'},
]

class TreatisesDisplay extends Component { //Sceglie i trattati
	render() {
		return (
			<Paper>
			  <div style={{display: 'flex', justifyContent: 'space-evenly', flexWrap: 'wrap'}}>
			    {treatises.map(
			  	  (x, index) => (
						  <div key={index}>
						    <span>{x.title+', '+x.author+', '+x.year}</span>
						    <Checkbox
									checked={this.props.query.treatises.includes(x)}
									onChange={()=>this.props.handleRead('treatises', x)}
									value="checkedB"
									color="primary"
						    />
						  </div>
						  )
			    )}
			  </div>
			</Paper>
		)
	  }
}

export default TreatisesDisplay
