import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

class Landing extends Component { // Pagina iniziale
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <h1 className='landingTitle'>MSearch</h1>
        <h2 className='landingSubtitle'>{this.props.language.landing_subtitle}</h2>
        <p className='landingExplanation'>
          {this.props.language.landing_explanation1}
          <br />
          {this.props.language.landing_explanation2}
        </p>
        <Button
          variant='raised'
          color='secondary'
          onClick={() => this.props.handleNavigation('command')}
        >
          {this.props.language.landing_button1}
        </Button>
      </div>
    )
  }
}

export default Landing
