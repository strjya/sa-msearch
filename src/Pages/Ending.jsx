import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import 'Assets/CSS/wizard.css'

class Ending extends Component { // Pagina iniziale
  render() {
    return (
      <div>
        <h2 className='endingMaintitle'>{this.props.language.ending_endingMaintitle}</h2>
        <h3 className='endingPill1'>{this.props.language.ending_endingPill1}</h3>
        <h3 className='endingPill2'>{this.props.language.ending_endingPill2}</h3>
      </div>
    )
  }
}

export default Ending
