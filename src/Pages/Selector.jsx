import React, { Component } from 'react'
import 'Assets/CSS/App.css';
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'

class Selector extends Component {
	constructor(props) {
    super(props)
    this.state = {values: {
						books: [{from: '', to: ''}],
						chapters: [{from: '', to: ''}],
						parts: [{from: '', to: ''}],
			}
		}
	}

	componentDidMount() { //caricare nello stato i valori dello stato del Wizard
		let inputValues = this.props.query.parameters
		let stateValues = {
												books: [{from: '', to: ''}],
												chapters: [{from: '', to: ''}],
												parts: [{from: '', to: ''}],
											}
		let counter = 0

		Object.keys(inputValues).map((x)=> {
			if (x==='books' || x==='chapters' || x==='parts') {
				for (let u in inputValues[x]) {
					if (stateValues[x][0].from==='') {stateValues[x][0].from = inputValues[x][u]}
					else {
						if (inputValues[x][u]===(stateValues[x][counter].from+1) ||
								inputValues[x][u]===(stateValues[x][counter].to+1))
								stateValues[x][counter].to = inputValues[x][u]
						else {
							stateValues[x].push({from:'', to: ''})
							counter++
							stateValues[x][counter].from = inputValues[x][u]
						}
					}
				}
			}
		})

		this.setState({values: stateValues})
	}

	handleRead(target, key, value) { //carica nello stato i valori dell'utente
		let readValues = this.state.values[this.props.page]
		if (value==='' && target==='from')
		 	readValues.splice(key, 1)
		else if (value==='' && target==='to')
			readValues[key][target] = ''
		else {
			readValues[key][target] = Number(value)
			if (readValues[key+1]===undefined)
				readValues.push({from:'', to: ''})
		}
		let values = this.state.values
		values[this.props.page]=readValues
		this.setState({values: values})
		this.handleWrite(readValues)
	}

	handleWrite(readValues) { //Estrarre dallo stato i valori da mandare al wizard
		let exitValues = []

		for (let v in readValues) { //Extract a simple numbers array from the this.state
			if (readValues[v].from!=='' && readValues[v].to==='')
				exitValues.push(readValues[v].from)
			if (readValues[v].from!=='' && readValues[v].to!=='')
				if (readValues[v].from<=readValues[v].to)
					for (let f=readValues[v].from; f<=readValues[v].to; f++)
						exitValues.push(f)
				else
					for (let f=readValues[v].to; f<=readValues[v].from; f++)
						exitValues.push(f)
		}

		exitValues.sort((a, b) => a - b) //Sort the array

		for (let j=0; j<exitValues.length; j++) //Delete duplicates
			if (exitValues[j]===exitValues[j-1]) {
				exitValues.splice(j, 1)
				j--
			}
		this.props.handleRead(this.props.page, exitValues) //Send the array to the wizard
	}

	render() {  //Sceglie i parametri in base alla pagina fornita
		let pageSubTitle= this.props.page + "_subTitle"
		
		return (
					<Grid container style ={{textAlign: 'center'}}>
						<Grid item xs = {12} md = {12}>
							<h2 className = 'landingSubtitle'>{this.props.language[pageSubTitle]}</h2>
						</Grid>
							{this.state.values[this.props.page].map((x, key)=>
								(<Grid item xs = {12} md = {4}>
									<Toolbar>
										<TextField
				              InputLabelProps={{
				                style: {
				                  color: 'rgba(255, 255, 255, 0.7)',
				                }
				              }}
											label= {(x.to===''? 'Num.':'From:')}
											type='number'
											inputProps={{min:0}}
											onChange={(event)=>this.handleRead('from', key, event.target.value)}
											value={x.from}
											style={{width: 150, margin: 5, opacity: (x.from==='' && key!==0 ? 0.4 : 1)}}
										/>
										<TextField
				              InputLabelProps={{
				                style: {
				                  color: 'rgba(255, 255, 255, 0.7)',
				                }
				              }}
											label= 'To:'
											type='number'
											inputProps={{min:0}}
											onChange={(event)=>this.handleRead('to', key, event.target.value)}
											value={x.to}
											style={{width:150, margin: 5, opacity: (x.from==='' ? 0 :(x.to==='' ? 0.4 : 1))}}
											disabled={x.from===''}
										/>
									</Toolbar>
								</Grid>)
							)}
		      </Grid>
			)
	}
}

export default Selector
