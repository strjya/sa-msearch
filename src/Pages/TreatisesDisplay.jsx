import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import Checkbox from '@material-ui/core/Checkbox'
import Typography from '@material-ui/core/Typography'
import Toolbar from '@material-ui/core/Toolbar'
import Axios from 'axios'
import api from 'Assets/api.json'

class TreatisesDisplay extends Component { // Sceglie i trattati
  constructor(props) {
    super(props)
    this.state = { treatises: [] }
  }

  componentDidMount() {
    Axios.post(api.url + '/getTreatisesList').then(response => {
      this.setState({ treatises: response.data })
    })
  }

  checked(x) {
    let treatisesArray = this.props.query.treatises
    for (let t in treatisesArray)
      if (x.title === treatisesArray[t].title &&
          x.author === treatisesArray[t].author &&
          x.year === treatisesArray[t].year)
        return true
    return false
  }

  render() {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-evenly', flexWrap: 'wrap' }}>
        {this.state.treatises.sort((a, b) => a.year - b.year).map(
          (x) => (
            <Toolbar style={{ width: 250 }}>
              <Checkbox
                checked={this.checked(x)}
                onChange={() => this.props.handleRead(this.props.page, x)}
                value='checkedB'
                color='secondary'
              />
              <Typography>{x.title + ', ' + x.author + ', ' + x.year}</Typography>
            </Toolbar>
          )
        )}
      </div>
    )
  }
}

export default TreatisesDisplay
