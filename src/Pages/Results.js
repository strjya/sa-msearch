import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import 'Assets/CSS/Results.css'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'
import Toolbar from '@material-ui/core/Toolbar'
import CircularProgress from '@material-ui/core/CircularProgress'
// Extractors
import IndexExtractor from 'Pages/Extractors/IndexExtractor.js'
import TextExtractor from 'Pages/Extractors/TextExtractor.js'
import TextPDFExtractor from 'Pages/Extractors/TextPDFExtractor.js'
import { animateScroll as scroll } from 'react-scroll'
import Tooltip from '@material-ui/core/Tooltip'
import Drawer from '@material-ui/core/Drawer'
import Fab from '@material-ui/core/Fab'

import ArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import InfoIcon from '@material-ui/icons/Info'
import PDFIcon from '@material-ui/icons/PictureAsPdf'
import CloseIcon from '@material-ui/icons/Close'

import headerLinksStyle from 'Assets/CSS/headerLinksStyle.jsx'
import PDFgenerator from 'Assets/PDFgenerator'

// Import to generate the pdf

/* to block user selection
CSS: -webkit-user-select: none;
     -moz-user-select: -moz-none;
      -ms-user-select: none;
          user-select: none;
React: onCopy, onCut messo su tutti i div che modifica il testo copiato (properietà clipboardData) in un "Copyright SwordAcademy 2018. Per ottenere i risultati della tua ricerca, usa il pulsante Download"
onSelect c'è un prevent default possibile

to create the downloadable pdf:
https://parall.ax/products/jspdf
https://github.com/MrRio/jsPDF
è lo stesso?
Imparare bene la libreria perché viene comoda!
*/

class Results extends Component {
  constructor(props) {
    super(props)
    this.PDFgenerator = new PDFgenerator()
    this.state = {
      indexIsOpen: false,
    }
    this.toggleIndex = this.toggleIndex.bind(this)
  }

  toggleIndex() {
    this.setState({ indexIsOpen: !this.state.indexIsOpen })
  }

  render() {
    if (this.props.loading)
      return (<Loading {...this.props} />)
    else
      return (
        <Grid container spacing={8}>
          <Grid item xs={12} className='results'>
            <div id='content'>
              <TextExtractor {...this.props} />
            </div>
          </Grid>
          <Drawer
            anchor='right'
            open={this.state.indexIsOpen}
            onClose={() => this.setState({ indexIsOpen: false })}
            PaperProps={{ style: { width: '70%' } }}
          >
            <div className='sidenav'>
              <h4 className='indexTitle'>{this.props.language.results_index}</h4>
              <IndexExtractor {...this.props} />
            </div>
          </Drawer>
          <Sidebar
            {...this.props}
            {...this.state}
            PDFgenerator={this.PDFgenerator}
            toggleIndex={this.toggleIndex}
          />
        </Grid>
      )
  }
}

function Sidebar(props) {
  return (
    <Toolbar className='navFooter'>
      <Fab
        color='secondary'
        onClick={props.toggleIndex}
        style={{ marginBottom: 15, marginTop: 15 }}
      >
        <InfoIcon />
      </Fab>
      <Fab
        variant='raised'
        color='secondary'
        style={{ marginBottom: 15, marginTop: 15 }}
        onClick={() => props.PDFgenerator.generatePDF(props.data, props.query)}
      >
        <PDFIcon />
      </Fab>
      <Fab
        variant='flat'
        color='primary'
        onClick={scroll.scrollToTop}
        style={{ marginTop: 15 }}
      >
        <ArrowUpIcon />
      </Fab>
    </Toolbar>
  )
}

function Loading(props) {
  return (
    <div className='loadingScreen'>
      <div>
        <CircularProgress size={50} color='secondary' />
        <h1 className='loadingTitle'>{props.language.loadingTitle}</h1>
        <h2 className='loadingSubtitle'>{props.language.loadingText}</h2>
      </div>
    </div>
  )
}
export default Results
