import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import 'Assets/CSS/Results.css'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Button from '@material-ui/core/Button'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Collapse from '@material-ui/core/Collapse'
import Paper from '@material-ui/core/Paper'
// import scroller
import { Link as SLink } from 'react-scroll'

// TODO: each title is white and bold if there is a hit, grey and italic if there isn't
class IndexExtractor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openKeys: {},
    }
  }

  handleClick(e) {
    let openKeys = {}
    let passedKeyArray = e.split(' - ')
    let startingKey = passedKeyArray[0]
    openKeys[startingKey] = (passedKeyArray.length === 1 ? !this.state.openKeys[startingKey] : true)
    for (let i = 1; i < passedKeyArray.length; i++) {
      startingKey += ' - ' + passedKeyArray[i]
      openKeys[startingKey] = (passedKeyArray.length - 1 === i ? !this.state.openKeys[startingKey] : true)
    }
    this.setState({ openKeys: openKeys })
  };

  extractBooksIndex(books, prevKey) {
    return (
      <List disablePadding>
        {Object.keys(books).map((key) => (
          <React.Fragment>
            <SLink activeClass='activeSLink' to={prevKey + ' - ' + key} spy smooth duration={1000} onClick={this.handleClick.bind(this, prevKey + ' - ' + key)}>
              <ListItem className='indexBookTitle'>
                <ListItemText
                  primary={this.props.language.Book + ' ' + key}
                  secondary={(
                    <div className='searchPillsWrapper'>
                      {Object.keys(books[key].keywords).map(x =>
                        (books[key].keywords[x] > 0 ? <span className='searchPill'>{x + ' ' + books[key].keywords[x]}</span> : null)
                      )}
                    </div>
                  )}
                />
                <ListItemIcon>
                  {this.state.openKeys[prevKey + ' - ' + key] ? <ExpandLess color='secondary' /> : <ExpandMore color='secondary' />}
                </ListItemIcon>
              </ListItem>
            </SLink>
            <Collapse in={this.state.openKeys[prevKey + ' - ' + key]} timeout='auto' unmountOnExit>
              <List disablePadding>
                {this.extractChaptersIndex(books[key].chapters, books[key], prevKey + ' - ' + key)}
              </List>
            </Collapse>
          </React.Fragment>
        ))}
      </List>
    )
  }

  extractChaptersIndex(chapters, book, prevKey) {
    return (
      <List disablePadding>
        {Object.keys(chapters).map((key) => (
          <React.Fragment>
            <SLink activeClass='activeSLink' to={prevKey + ' - ' + key} spy smooth duration={1000} onClick={this.handleClick.bind(this, prevKey + ' - ' + key)}>
              <ListItem className='indexChapterTitle'>
                <ListItemText
                  primary={this.props.language.Chapter + ' ' + key}
                  secondary={(
                    <div>
                      <div>
                        {chapters[key].title}
                      </div>
                      <div className='searchPillsWrapper'>
                        {Object.keys(chapters[key].keywords).map(x =>
                          (chapters[key].keywords[x] ? <span className='searchPill'>{x + ' ' + chapters[key].keywords[x]}</span> : null)
                        )}
                      </div>
                    </div>
                  )}
                />
                {Object.keys(chapters[key].parts).length > 1 ? (
                  <ListItemIcon>
                    {this.state.openKeys[prevKey + ' - ' + key] ? <ExpandLess color='secondary' /> : <ExpandMore color='secondary' />}
                  </ListItemIcon>
                ) : null}
              </ListItem>
            </SLink>
            <Collapse in={this.state.openKeys[prevKey + ' - ' + key]} timeout='auto' unmountOnExit>
              <List disablePadding>
                {this.extractPartsIndex(chapters[key].parts, prevKey + ' - ' + key)}
              </List>
            </Collapse>
          </React.Fragment>
        ))}
      </List>
    )
  }

  extractPartsIndex(parts, prevKey) {
    if (Object.keys(parts).length > 1)
      return (
        <List disablePadding>
          {Object.keys(parts).map((key) => (
            <SLink activeClass='activeSLink' to={prevKey + ' - ' + key} spy smooth duration={1000}>
              <ListItem className='indexPartTitle'>
                <ListItemText
                  primary={this.props.language.Part + ' ' + key}
                  secondary={(
                    <div>
                      <div>
                        {parts[key].title}
                      </div>
                      <div className='searchPillsWrapper'>
                        {Object.keys(parts[key].keywords).map(x =>
                          (parts[key].keywords[x] ? <span className='searchPill'>{x + ' ' + parts[key].keywords[x]}</span> : null)
                        )}
                      </div>
                    </div>
                  )}
                />
              </ListItem>
            </SLink>
          ))}
        </List>
      )
    else return null
  }

  render() {
    return (
      <Paper>
        <List disablePadding style={{ paddingBottom: 70 }}>
          {Object.keys(this.props.data).map((key) => // Map the treatises searched
            (Object.keys(this.props.data[key].books).length !== 0) // Check if there are results
              ? (
                <React.Fragment>
                  <SLink activeClass='activeSLink' to={this.props.data[key].author + this.props.data[key].title + this.props.data[key].year} spy smooth duration={1000} onClick={this.handleClick.bind(this, key)}>
                    <ListItem className='indexTreatiseTitle'>
                      <ListItemText
                        primary={this.props.data[key].author + ' - ' + this.props.data[key].title + ' - ' + this.props.data[key].year}
                        secondary={<div className='searchPillsWrapper'>{Object.keys(this.props.data[key].keywords).map(x => <span className='searchPill'>{x + ' ' + this.props.data[key].keywords[x]}</span>)}</div>}
                      />
                      <ListItemIcon>
                        {this.state.openKeys[key] ? <ExpandLess color='secondary' /> : <ExpandMore color='secondary' />}
                      </ListItemIcon>
                    </ListItem>
                  </SLink>
                  <Collapse in={this.state.openKeys[key]} timeout='auto' unmountOnExit>
                    <List disablePadding>
                      {this.extractBooksIndex(this.props.data[key].books, this.props.data[key].author + this.props.data[key].title + this.props.data[key].year)}
                    </List>
                  </Collapse>
                </React.Fragment>
              )
              : (
                <ListItem className='ExpandLess'>
                  <SLink activeClass='activeSLink' to={this.props.data[key].author + this.props.data[key].title + this.props.data[key].year} spy smooth duration={1000}>
                    <ListItemText primary={this.props.data[key].author + ' - ' + this.props.data[key].title + ' - ' + this.props.data[key].year} />
                  </SLink>
                </ListItem>
              )
          )}
        </List>
      </Paper>
    )
  }
}

export default IndexExtractor
