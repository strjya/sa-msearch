import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper'
// Icons
import ArrowUp from '@material-ui/icons/ArrowUpward'
// import scriller
import { Element } from 'react-scroll'

class TextExtractor extends Component {
  extractBooks(books, prevKey) {
    return (
      <React.Fragment>
        {Object.keys(books).map((key) => (
          <Element name={prevKey + ' - ' + key}>
            <div className='bookTitleWrapper'>
              <h2 className='bookNumber'>{this.props.language.Book + ' ' + books[key].number}</h2>
              <h2 className='bookTitle'>{books[key].title}</h2>
            </div>
            <div className='bookContainer'>
              {this.extractChapters(books[key].chapters, prevKey + ' - ' + key)}
            </div>
          </Element>
        ))}
      </React.Fragment>
    )
  }

  extractChapters(chapters, prevKey) {
    return (
      <React.Fragment>
        {Object.keys(chapters).map((key) => (
          <Element name={prevKey + ' - ' + key}>
            <div className='chapterTitleWrapper'>
              {key != 0
                ? <h1 className='chapterNumber'>{chapters[key].number}</h1>
                : null}
              <h3 className='chapterTitle'>{chapters[key].title}</h3>
            </div>
            <div className='chapterContainer'>
              {this.extractParts(chapters[key].parts, prevKey + ' - ' + key)}
            </div>
          </Element>
        ))}
      </React.Fragment>
    )
  }

  extractParts(parts, prevKey) {
    let singlePart = Object.keys(parts).length <= 1
    return (
      <div>
        {Object.keys(parts).map((key) => (
          <Element name={prevKey + ' - ' + key}>
            {!singlePart && parts[key].title !== ''
              ? (
                <div className='partTitleWrapper'>
                  <h3 className='partTitle'>{parts[key].title}</h3>
                  <h3 className='partNumber'>
                    {this.props.language.Part + ' ' + parts[key].number}
                  </h3>
                </div>
              ) : null}
            <div className='partContainer'>
              {this.extractText(parts[key], prevKey + ' - ' + key)}
            </div>
          </Element>
        ))}
      </div>
    )
  }

  extractText(part) { // choose how to extract the Text from the parts
    switch (this.props.query.command) {
      case 'search':
        return <p>{this.composeText(part.text)}</p>
      case 'view':
        return part.text.split(/\r?\n/g).map(x => (<p>{x}</p>))
      default:
        return ''
    }
  }

  composeText(text) { // Render [text]
    return (
      <p>
        {text.map((x, index, array) => {
          if (x.hidden) return ''
          let word = (x.keyword ? (
            <span className='keyword'>{x.word + ' '}</span>)
            : x.word + (array[index + 1]
              ? (x.word === '\'' || array[index + 1].word === '\'' ? '' : ' ')
              : ' '))
          if (x.word.match(/\r?\n/)) return <div className='fakeParagraph' />
          return word
        })}
      </p>
    )
  }

  render() {
    return (
      <div>
        {Object.keys(this.props.data).map((key) => // Map the treatises searched
          (Object.keys(this.props.data[key].books).length !== 0) // Check if there are results
            ? (
              <Element name={this.props.data[key].author + this.props.data[key].title + this.props.data[key].year}>
                <Paper className='treatisePaper'>
                  <h1 className='treatiseTitle'>
                    {this.props.data[key].author}
                    {' '}
-
                    {' '}
                    {this.props.data[key].title}
                    {' '}
-
                    {' '}
                    {this.props.data[key].year}
                  </h1>
                  <div className='treatiseContainer'>
                    {this.extractBooks(this.props.data[key].books, this.props.data[key].author + this.props.data[key].title + this.props.data[key].year)}
                  </div>
                </Paper>
              </Element>
            )
            : (
              <Element name={this.props.data[key].author + ' - ' + this.props.data[key].title + ' - ' + this.props.data[key].year}>
                <Paper>
                  <h1 className='empty treatiseTitle'>
                    {this.props.data[key].author}
                    {' '}
-
                    {' '}
                    {this.props.data[key].title}
                    {' '}
-
                    {' '}
                    {this.props.data[key].year}
                  </h1>
                  <p style={{ color: 'red' }}>{this.props.language.results_emptyTreatise}</p>
                </Paper>
              </Element>
            )
        )}
      </div>
    )
  }
}

export default TextExtractor
