function TextPDFExtractor(props) {
  function extractTreatises() { // Create the text
    let data = props.data
    let keys = Object.keys(data)
    let treatisesResults = '<h2>Risultati</h2>'
    for (const key of keys)
      treatisesResults = treatisesResults + '<a name=' + key + '></a>' +
             '<h1>' + data[key].author + ' - ' + data[key].title + ' - ' + data[key].year + '</h1>' +
             extractBooks(data[key].books)

    return treatisesResults
  }

  function extractBooks(books) {
    let keys = Object.keys(books)
    let booksResults = ''
    for (const key of keys)
      booksResults = booksResults + '<a name=' + key + '></a>' +
            '<h2> Libro ' + books[key].number + '</h2>' +
            extractChapters(books[key].chapters)

    return booksResults
  }

  function extractChapters(chapters) {
    let keys = Object.keys(chapters)
    let chaptersResults = ''
    for (const key of keys)
      chaptersResults = chaptersResults + '<a name=' + key + '></a>' +
            '<h3> Capitolo ' + chapters[key].number + ' ' + chapters[key].title + '</h3>' +
            '<a href=#top>Top</a>' +
            extractParts(chapters[key].parts)

    return chaptersResults
  }

  function extractParts(parts) {
    let keys = Object.keys(parts)
    let partsResults = ''
    switch (props.query.command) {
      case 'view':
        for (const key of keys)
          partsResults = partsResults + '<a name=' + key + '></a>' +
                '<h3> Parte ' + parts[key].number + ' ' + parts[key].title + '</h3>' +
                '<p>' + parts[key].text + '</p>'

        break
      case 'search':
        for (const key of keys)
          partsResults = partsResults + '<h3> Parte ' + parts[key].number + ' ' + parts[key].title + '</h3>' +
                                      '<p>' + extractTexts(parts[key].text) + '</p>'

        break
    }
    return partsResults
  }

  function extractTexts(texts) { // Extract the texts for the "search" command
    let openedParagraph
    let textsResults = ''
    for (let t in texts)
      if (!texts[t].hidden) {
        openedParagraph = true
        if (texts[t].keyword)
          textsResults = textsResults + '<em>' + texts[t].word + '</em> '
        else
          textsResults = textsResults + '' + texts[t].word + ' '
      } else if (openedParagraph) {
        openedParagraph = false
        textsResults = textsResults + '</p><p>'
      } else { textsResults = textsResults + '' }

    return textsResults
  }

  return (
    extractTreatises()
  )
}

export default TextPDFExtractor
