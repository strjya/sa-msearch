import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

class CommandPage extends Component {
  render() {
    return (
      <Grid container>
        <Grid item xs={4} style={{ textAlign: 'center' }}>
          <Button
            variant='raised'
            color='secondary'
            onClick={() => {
              this.props.handleRead('command', 'view')
              this.props.handleNavigation('treatises')
            }}
          >
            VIEW
          </Button>
          <Typography className='text' style={{ marginTop: 15, textAlign: 'center' }}>{this.props.language.commandPage_view_text}</Typography>
        </Grid>
        <Grid item xs={4} style={{ textAlign: 'center' }}>
          <Button
            variant='raised'
            color='secondary'
            onClick={() => {
              this.props.handleRead('command', 'search')
              this.props.handleNavigation('treatises')
            }}
          >
            SEARCH
          </Button>
          <Typography className='text' style={{ marginTop: 15, textAlign: 'center' }}>{this.props.language.commandPage_search_text}</Typography>
        </Grid>
        <Grid item xs={4} style={{ textAlign: 'center' }}>
          <Button variant='raised' color='secondary' disabled>
            CONTEXT
          </Button>
          <Typography className='text' style={{ marginTop: 15, textAlign: 'center' }}>{this.props.language.commandPage_context_text}</Typography>
        </Grid>
      </Grid>
    )
	  }
}

export default CommandPage
