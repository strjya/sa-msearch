import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

class Keywords extends Component { // Scelta delle parole chiave e dei parametri collegati
  render() {
    return (
      <Grid container style={{ paddingLeft: 30, paddingRight: 30 }}>
        <Grid item xs={12} style={{ textAlign: 'center' }}>
          <Typography>{this.props.language.keywords_subTitle}</Typography>
        </Grid>
        <Grid item xs={12} sm={9} style={{ padding: 15 }}>
          {this.props.query.keywords.or.map( // visualizzazione dei textfield
            (x, key) => (
              <div style={{
                opacity: (!this.props.query.keywords.or[key + 1] && key !== 0 ? 0.4 : 1),
              }}
              >
                <TextField
                  InputLabelProps={{
                    style: {
                      color: 'rgba(255, 255, 255, 0.7)',
                    },
                  }}
                  id='keywords'
                  label={this.props.language.keywords_label}
                  margin='normal'
                  key={key}
                  value={x.and.toString().replace(/,/g, ' ')}
                  onChange={(event) => this.props.handleRead('keywords', event.target.value, key)}
                  onBlur={(event) => this.props.handleRead('keywords', event.target.value.trim(), key)}
                  fullWidth
                />
              </div>
            )
          )}
        </Grid>
        <Grid item xs={12} sm={3} style={{ padding: 15 }}>
          <div hidden={this.props.query.keywords.or[0].and.length < 1}>
            <div>
              <TextField
                InputLabelProps={{
                  style: {
                    color: 'rgba(255, 255, 255, 0.7)',
                  },
                }}
                type='number'
                inputProps={{ min: 0 }}
                id='range'
                label='Range'
                helperText={this.props.language.keywords_range_subTitle}
                margin='normal'
                value={this.props.query.parameters.range}
                onChange={(event) => this.props.handleRead('range', event.target.value)}
                fullWidth
              />
            </div>
            <div>
              <TextField
                InputLabelProps={{
                  style: {
                    color: 'rgba(255, 255, 255, 0.7)',
                  },
                }}
                type='number'
                inputProps={{ min: 0, max: 2 }}
                id='flexibility'
                label='Flexibility'
                margin='normal'
                helperText={this.props.language.keywords_flexibility_subTitle}
                value={this.props.query.parameters.flexibility}
                onChange={(event) => this.props.handleRead('flexibility', event.target.value)}
                fullWidth
              />
            </div>
            <div>
              <TextField
                InputLabelProps={{
                  style: {
                    color: 'rgba(255, 255, 255, 0.7)',
                  },
                }}
                type='number'
                inputProps={{ min: 0 }}
                id='distance'
                label='Distance'
                margin='normal'
                helperText={this.props.language.keywords_distance_subTitle}
                value={this.props.query.parameters.distance}
                onChange={(event) => this.props.handleRead('distance', event.target.value)}
                fullWidth
              />
            </div>
          </div>
        </Grid>
      </Grid>
    )
  }
}

export default Keywords
