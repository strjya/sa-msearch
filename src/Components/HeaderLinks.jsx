/* eslint-disable */
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// material-ui components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import CloseIcon from '@material-ui/icons/Close'
import HelpIcon from '@material-ui/icons/Help'

// @material-ui/icons
import { Apps, CloudDownload } from "@material-ui/icons";

// core components
import Button from "Components/CustomButtons/Button.jsx";
import IconButton from "Components/CustomButtons/IconButton.jsx";

import headerLinksStyle from "Assets/CSS/headerLinksStyle.jsx";

function HeaderLinks({ ...props }) {
  const { classes } = props;
  return (
    <List className={classes.list}>
      {(props.language.language !== "it") ?
        <ListItem className={classes.listItem}>
          <Button
            color="transparent"
            className={classes.navLink}
            onClick = {()=>props.handleLanguage("it")}
          >
            <img src={require("Assets/img/Icons/it.png")} />
          </Button>
        </ListItem>
      : null}
      {(props.language.language !== "en") ?
        <ListItem className={classes.listItem}>
          <Button
            color="transparent"
            className={classes.navLink}
            onClick = {()=>props.handleLanguage("en")}
          >
            <img src={require("Assets/img/Icons/uk.png")} />
          </Button>
        </ListItem>
      : null}
      <ListItem className={classes.listItem}>
          <Button
            color="transparent"
            className={classes.navLink}
            style={{color: 'white'}}
            onClick = {() => props.handleRead('reset')}
          >
            {props.language.header_links01}
          </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link to="/tutorial" className={classes.dropdownLink}>
          <Tooltip
            id="instagram-facebook"
            title={props.language.header_links02_title}
            placement={window.innerWidth > 959 ? "top" : "left"}
            classes={{ tooltip: classes.tooltip }}
          >
            <Button
              color="transparent"
              className={classes.navLink}
              style={{padding:13, color: 'white'}}
            >
              {props.language.header_links02} <HelpIcon style= {{marginLeft: 10}}/>
            </Button>
          </Tooltip>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <a href="http://sword.academy" className={classes.dropdownLink}>
          <Button
            color="transparent"
            className={classes.navLink}
            style={{padding:13, color: 'white'}}
          >
            {props.language.header_links03} <CloseIcon style= {{marginLeft: 10}}/>
          </Button>
        </a>
      </ListItem>
    </List>
  );
}

export default withStyles(headerLinksStyle)(HeaderLinks);
