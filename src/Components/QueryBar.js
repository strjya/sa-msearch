import React, { Component } from 'react'
import 'Assets/CSS/App.css'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Toolbar from '@material-ui/core/Toolbar'

class QueryBar extends Component {
  render() {
    return (
      <div>
        <Paper style={{ padding: 10 }}>
          <Toolbar>
            <TextField
              InputLabelProps={{
                style: {
                  color: ((this.props.queryError !== false && this.props.queryString !== '')
                    ? '#ff5131' : '#ffffff'),
                },
              }}
              id='query'
              label='Query'
              value={this.props.queryString}
              onChange={(event) => {
                let data = event.target
                data.value = data.value
                  .replace(/\s+/g, ' ')
                  .replace(/-+/g, '-')
                  .replace(/\.\.+/g, '..')
                  .toLowerCase()
                this.props.handleRead('query', data)
              }}
              margin='normal'
              fullWidth
              helperText={this.props.queryString === ''
                ? this.props.language.queryBar_helperText01
                : (this.props.queryError === true
                  ? this.props.language.queryBar_helperText02
                  : this.props.language[this.props.queryError])}
              error={this.props.queryError !== false && this.props.queryString !== ''}
              onFocus={() => this.props.handleNavigation('queryTreatises')}
              onKeyPress={(ev) => {
                if (ev.key === 'Enter') {
                  if (!this.props.queryError || !this.props.queryString === '') {
                    this.props.handleQuery()
                    document.activeElement.blur()
                  }
                  // and also everytime block the creation of a new line
                  ev.preventDefault()
                }
              }}
              onKeyDown={(ev) => {
                if (ev.key === 'Tab') {
                  this.props.handleRead('query', this.props.autoComplete(ev.target))
                  ev.preventDefault()
                }
              }}
              multiline
            />
            <Button
              variant='raised'
              style={{ marginLeft: 15 }}
              onClick={() => this.props.handleQuery()}
              disabled={this.props.queryError !== false || this.props.queryString === ''}
            >
              {this.props.language.queryBar_searchButton}
            </Button>
          </Toolbar>
        </Paper>
      </div>
    )
  }
}

export default QueryBar
