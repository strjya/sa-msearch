import React from 'react'
import 'Assets/CSS/App.css'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import HomeIcon from '@material-ui/icons/Home'
import ForwardIcon from '@material-ui/icons/ArrowForwardIos'

function BottomNavigation(props) { // Riceve da ContentManager i valori dei bottoni
  let disabled = !props.forwardButton ||
							(props.page === 'treatises' && props.query.treatises.length === 0) ||
							(props.page === 'keywords' && props.query.keywords.or.length === 1)
  return (
    <div style={{ marginTop: 30, display: 'flex', justifyContent: 'space-between', padding: 0, paddingRight: 10, paddingLeft: 10 }}>
      <Button // Reset di tutto.
        variant='flat'
        onClick={() => props.handleRead('reset')}
      >
        <HomeIcon style={{ marginRight: 10 }} />
        {' '}
        {props.language.header_links01}
      </Button>

      <Button // Forward.
        disabled={disabled}
        color='secondary'
        variant={disabled ? 'flat' : 'raised'}
        onClick={() => props.handleNavigation(props.forwardButton)}
        style={{ visibility: (props.forwardButton ? 'visible' : 'hidden') }}
      >
        {props.language[props.forwardButton]}
        <ForwardIcon style={{ marginLeft: 10 }} />
      </Button>
    </div>
  )
}

export default BottomNavigation
