import React from 'react'
import 'Assets/CSS/App.css'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import HomeIcon from '@material-ui/icons/Home'
import BackIcon from '@material-ui/icons/ArrowBackIos'

function TopNavigation(props) { // Riceve da ContentManager i valori dei bottoni
  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-between', padding: 0, paddingRight: 10, paddingLeft: 10, flexWrap: 'wrap' }}>
        <Button
          disabled={!props.backwardButton}
          variant='flat'
          onClick={() => props.handleNavigation(props.backwardButton)}
          style={{ visibility: (props.backwardButton ? 'visible' : 'hidden') }}
        >
          <BackIcon style={{ marginRight: 10 }} />
          {props.language[props.backwardButton]}
        </Button>
      </div>
      <h1 className='title'>{props.title}</h1>
    </div>
  )
}

export default TopNavigation
