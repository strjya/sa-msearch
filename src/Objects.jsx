import React, { Component } from 'react'
import './App.css'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'

class Objects extends Component {
	constructor(props) {
    super(props)
    this.state = {
			objects: [null],
		}
	}

	handleChange = (index, value) => {
		let objects = this.state.objects
		if (index === objects.length-1 && objects[index] === null)
			this.newTextField()
		objects[index] = value
		this.setState(objects: objects)

	}

	newTextField() {  //Funzione che aumenta il numero dei textfield
		let objects = this.state.objects
		objects.push(null)
	  this.setState({objects: objects})
	}

	render() {
	  return (
			<Grid container>
        <Grid item xs={2}>
				</Grid>
        <Grid item xs={6}>
			    {
						this.state.objects.map(  //visualizzazione dei textfield
						(x,key) => (
						  <div style={{
								opacity: (x===null? 0.4 : 1)
							}}>
					 		  <span>Frase da cercare:</span>
								<TextField
					     		id = 'object'
					     		label = 'object'
					     		margin = 'normal'
									key = {key}
									value = {x}
									onChange = {(event)=>this.handleChange(key,event.target.value)}
					     	/>
					 		</div>
						)
					)}
				</Grid>
        <Grid item xs={4}>
				</Grid>
			</Grid>
	  )
	}
}


export default Objects
