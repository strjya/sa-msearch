import Wizard from 'Wizard'

var indexRoutes = [
  { path: "*", name: "Wizard", component: Wizard },
];

export default indexRoutes;
